package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.bean.AdminLogin;
import com.bean.Items;
import com.bean.LoginUser;
import com.dao.AdminLogRepository;
import com.dao.ItemRepository;
import com.dao.UserRepository;




@SpringBootApplication
public class AngularMiniproject1Application implements CommandLineRunner {

	public static void main(String[] args) {
		
		SpringApplication.run(AngularMiniproject1Application.class, args);
		System.err.println("server running on 8080");
	}
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ItemRepository bookRepository;
	
	@Autowired
	private AdminLogRepository adminLogRepository;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void run() {
		
	}
	@Override
	public void run(String... args) throws Exception {
		String sql="set FOREIGN_KEY_CHECKS=0";
		int result=jdbcTemplate.update(sql);
	}
	
	@Bean
	public void inserData(){
		LoginUser login1=new LoginUser("Ramya","goud","ramya@gmail.com","95356488999","ramya","123456");
		userRepository.save(login1);
		
		LoginUser login2=new LoginUser("Soumya","reddy","soumya@gmail.com","999945783","soumya","123456");
		userRepository.save(login2);
		
		LoginUser login3=new LoginUser("Harish","N","harish@gmail.com","888856799","harish","78903");
		userRepository.save(login3);
		
		
		
		AdminLogin admin1=new AdminLogin("Haritha", "polo", "haritha@gmail.com", "9999986866", "haritha", "haritha");
		adminLogRepository.save(admin1);

		Items book=new Items("Veg Roll","80","3","IMG001");
		bookRepository.save(book);
		
		Items book1=new Items("Burgur","100","1","IMG002");
		bookRepository.save(book1);
		
		Items book2=new Items("Samosa","20","1","IMG003");
		bookRepository.save(book2);
		
		Items book3=new Items("South India Meal ","150","3","IMG004");
		bookRepository.save(book3);
		
		Items book4=new Items("Parata","50","2","IMG005");
		bookRepository.save(book4);
		
		
	}


}
